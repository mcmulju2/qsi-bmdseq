% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/moveEnsembleIDsToRownames.R
\name{moveEnsembleIDsToRownames}
\alias{moveEnsembleIDsToRownames}
\title{Return a matrix with Ensemble ID column moved to rownames}
\usage{
moveEnsembleIDsToRownames(countMatrix, geneID)
}
\arguments{
\item{countMatrix}{the data matrix (character) with Ensemble IDs as a column}

\item{geneID}{a character vector with the name of the Ensemble ID column that matches to geneID}
}
\value{
A matrix with the matched column as rownames
}
\description{
Helper method in BMDseq designed to detect an "id" column and move its values to rownames.
}
