#' Create combined BMD Annotation data.frame from all annotation files found at path
#'
#' Helper method in BMDseq designed to get all annotation files from the server
#' and assemble them via rbind() into one annotation frame
#'
#' @param annoFilePath path to annotation files on Linux server
#' @param filePattern file extension to be collected and combined
#' @param subjectID string name of field to use as Subject IDs
#' @param timeText string name of field used to find the sample Time Text
#' @param check.names Bool should data frame names be checked when reading annotation files.
#' @return A data.frame of the combined annotation entries
#' @export

assembleAnnotationFiles <- function(annoFilePath, filePattern="*.txt",
                                    subjectID = "Subject.ID",
                                    stsHeader = "Novartis.Tracking.ID",
                                    timeText = "Replicate.Group",
                                    check.names = T){

  #Get all files matching filePattern argument, return their count, and return their file names
  annotationFileList <- list.files(path = annoFilePath,
                                   pattern = filePattern,
                                   ignore.case = TRUE)
  #Sanity check: Did the expected file(s) exist at the location? [list.files used]
  if(length(annotationFileList) == 0){stop("No annotation files found.  Check that file exists at path.\n")}

  #Announce number of files and their names to console
  cat(paste('Annotation file assembler returning ', length(annotationFileList),
            ' files:\n', sep = ""))
  for(i in 1:length(annotationFileList)){
    cat(annotationFileList[i],'\n')
  }

  returnAnnotationFrame <- combineDataFramesFromFileList(dataFilePath = annoFilePath,
                                                         annotationFileList = annotationFileList,
                                                         checkNames = check.names,
                                                         extendNAs = TRUE,
                                                         removeNAs = TRUE)


  # Test if subjectID argument can be found in data frame and create the
  # SubjectId field from it if successful
  returnAnnotationFrame <- getSubjectIdWrapper(annotationFrame = returnAnnotationFrame,
                                               subjectID = subjectID)

  # Test if Novartis.Tracking.ID can be found in data frame and create the NovID column from it if successful
  returnAnnotationFrame <- getNovIDFromAnnotationFile(returnAnnotationFrame, stsHeader)

  ## Eliminate duplicate records for each donor
  ## Also relies on hard-coded $NovID field produced in above
  ## getNovID_From_Annotation_File method call
  returnAnnotationFrame <- eliminateDuplicateAnnotations(returnAnnotationFrame)

  # Move STS sample IDs to rownames
  ## Relies on $NovID field created above
  returnAnnotationFrame <- copySampleIdsToRownames(returnAnnotationFrame)

  ## Parce clinical Time Text codes for study samples
  returnAnnotationFrame <- parseSampleAndTimeText(annotationFrame = returnAnnotationFrame,
                                                  columnHeader = "Replicate.Group")

  return(returnAnnotationFrame)
}
