#' Parse the clinical study Time Text from a given column of Annotation data
#'
#' Helper method in BMDseq designed to  parse the 'columnHeader' column from an
#' Annotation data.frame, and evaluate the 2nd and 3rd elements.
#' Any time text not 3 characters long will return 'Not Found'.  Otherwise, the
#' appropriate text will be added to the annotationFrame.
#'
#' @param annotationFrame input data.frame of combined annotation files (data.frame)
#' @param columnHeader String reference to column name containing Time Text
#' @return A dataframe of the Annotations with the new Time Text column added


parseSampleAndTimeText <- function(annotationFrame, columnHeader = "Replicate.Group"){

  #Sanity check: Does the subjectID column exist?  If no, stop.
  stringToSplit <- tryCatch(expr = annotationFrame[,eval(columnHeader)], error = function(caughtError) {
    # error handler picks up where error was generated
    message(paste("parseSampleAndTimeText experienced the following error: ",caughtError))

    stop(paste0("Unable to detect ", columnHeader, " column.\n",
                "SampleCode and TimeText will not be added to the AnnotationFrame.\n"))
    })

  parseList <- sapply(X = stringToSplit, FUN = function(X){
    splitString <- unlist(strsplit(x = X, split = "_"))

    list(VisitNumber = if(nchar(splitString[2]) == 3) splitString[2] else "Not Found",
         TimeText = if(nchar(splitString[3]) == 3) splitString[3] else "Not Found"
    )
    })

  annotationFrame$VisitNumber <- unlist(parseList[1,])
  annotationFrame$TimeText <- unlist(parseList[2,])

  return(annotationFrame)
}
