#' Extract Novartis Tracking ID from BMD annotation file
#'
#' This function extracts the Novartis tracking ID from the annotation file of same column name
#'
#' @param annotationFrame Annotation file input (data frame)
#' @param stsHeader name of column containing STS labels
#' @return A matrix of the annotation file with Nov IDs column (data frame)


getNovIDFromAnnotationFile <- function(annotationFrame, stsHeader){
  if(stsHeader %in% colnames(annotationFrame)){
    if(class(annotationFrame) != "data.frame"){
      annotationFrame <- as.data.frame(annotationFrame)
    }
    annotationFrame$NovID <- as.character(annotationFrame$Novartis.Tracking.ID)
    annotationFrame$NovID <- sapply(X=annotationFrame$NovID,FUN=function(X) {
      gsub(pattern = " ", replacement = "",x = X)
    })
  }else{
    stop(paste0("Annotation data frame does not contain column ", stsHeader,".\n"))
  }
  #Changing data types, so this needs to be cut if data.frame works.
  #annotationFrame <- as.matrix(annotationFrame)
  return(annotationFrame)
}
