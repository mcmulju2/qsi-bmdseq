#' Return Boxplot with custom formatting
#'
#' Method creates a boxplot comparing two variables across Subjects
#'
#' @param metaPlotData Input meta plot data frame
#' @param meltedData Input heatmap melted Counts data frame
#' @param plotVarX String reference for field to be plotted on X-Axis
#' @param metaVarY String reference for field to be plotted on meta plot Y-Axis
#' @param heatVarY String reference for field to be plotted on heat plot Y-Axis
#' @param heatFill String reference for field to be plotted as fill on heatmap
#' @param plotTitle String title of plot over all
#' @param metaAxisLabels Vector of 2 labels for meta plot x and y axes
#' @param heatAxisLabels Vector of 2 labels for heat plot x and y axes
#' @param heatmapMetaPlotTheme Theme for meta plot
#' @param heatmapCountPlotTheme Theme for heat plot
#' @param heatmapScaleFill Palette for heatmap scale fill
#' @param plotHeights plotgrid() dimensions of plot
#' @param writePlot Bool save out plots to file
#' @param savePlotDims Vector of plot save file dimensions (Defaults to 1080x1080)
#' @param projectPath String path of project on R server
#'
#' @return Returns plot
#'
#' @export

heatmapMetaPlot <- function(metaPlotData, meltedData, plotVarX, metaVarY,
                            heatVarY, heatFill, plotTitle="GiveMeANiceNamePlease",
                            metaAxisLabels = c("", ""), heatAxisLabels = c("", ""),
                            showHeatLegend = T,
                            heatmapMetaPlotTheme, heatmapPlotTheme, heatmapScaleFill,
                            plotHeights = c(1,8),
                            writePlot = F, savePlotDims = c(1080, 1080),
                            saveFileName = "",
                            projectPath=NULL){
  require(ggplot2)

  metaPlot <- ggplot(data = metaPlotData, aes_string(x=plotVarX, y=metaVarY)) +
  ggtitle(label = plotTitle) +
  geom_tile(color = "white", size = 0.25) +
  labs(x = metaAxisLabels[1], y = metaAxisLabels[2]) +
  scale_x_discrete(labels = element_blank()) +
  coord_fixed(ratio = .5) +
  heatmapMetaPlotTheme

  heatmapPlot <- ggplot(data = meltedData,
                        aes_string(x = plotVarX, y= heatVarY, fill = heatFill)) +
    labs(x = heatAxisLabels[1], y = heatAxisLabels[2]) +
    geom_tile(colour = "white", size = 0.25, show.legend = showHeatLegend) +
    heatmapScaleFill +
    heatmapPlotTheme

  heatmapMetaPlot <- plot_grid(metaPlot, heatmapPlot, ncol = 1,
                               rel_heights = plotHeights, align = 'v', axis = 'l')

  if(writePlot){
    if(!is.null(projectPath)){
      if(saveFileName == ""){
        saveFileName <- gsub(plotTitle, pattern = "[[:punct:]]",replacement = " ")
        saveFileName <- gsub(plotTitle, pattern = " ",replacement = "_")
      }
      png(filename = paste(projectPath, paste0("Plots/",saveFileName,".png"),
                           sep ="/"), width = savePlotDims[1], height = savePlotDims[2])
      plot(heatmapMetaPlot)
      invisible(dev.off())
      }
    }

  return(heatmapMetaPlot)
  }
