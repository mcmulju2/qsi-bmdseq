#' Return Boxplot with custom formatting
#'
#' Method creates a boxplot comparing two variables across Subjects
#'
#' @param myData Input data frame
#' @param myLabel String of plot name for use in saving file and plot title
#' @param projectPath Path to where project is stored
#' @param myYlab String of y-axis label
#' @param id.vars List of strings indicating data fields to be plotted
#' @param trans Transformation type to be applied to values
#' @param overwritePlots bool if plots should be overwritten
#' @param width Width of plot
#' @param height Height of plot
#' @param writeFigure bool if plot should be saved to file
#' @param colorPalette custom color palette
#' @param legendPosition where to place legend, defaults to "none"
#' @return Returns plot
#' @export

comparisonBoxPlot <- function(myData, myLabel="GiveMeANiceNamePlease", projectPath=NULL,
                              myYlab = "Log2 fpkm", id.vars= c("Subject","Batch"), trans="log2",
                              overwritePlots=T, width = 720, height = 580, writeFigure=T,
                              colorPalette, legendPosition = "None"){

  melt_filt_refEdata_t <- melt(data = myData, id.vars = id.vars)
  melt_filt_refEdata_t$value < as.numeric(melt_filt_refEdata_t$value)
  if(trans%in%c("log2","log","log10")) melt_filt_refEdata_t <- melt_filt_refEdata_t[melt_filt_refEdata_t$value>0,]
  referenceSampleBarplot<-ggplot(data = melt_filt_refEdata_t, aes_string(x = id.vars[1], y = "value", fill=id.vars[2]))+
    geom_boxplot() +
    scale_fill_manual(values=colorPalette) +
    ggtitle(label = myLabel) +
    ylab(myYlab) +
    scale_y_continuous(trans=trans) +
    theme(plot.title = element_text(size = rel(1.2)),
          legend.position= legendPosition,
          axis.text.x=element_text(colour="black", angle = 90, vjust = 0.5, size = rel(0.9)),
          axis.title.x = element_blank())

  if(writeFigure){
    myLabel <- gsub(myLabel,pattern="[[:punct:]]",replacement = "")
    myLabel <- gsub(myLabel,pattern=" ",replacement = "_")
    if(!is.null(projectPath)){
      if(overwritePlots || !file.exists(paste(projectPath,paste0("Plots/",myLabel,".png"),
                                              sep ="/"))){
        png(filename = paste(projectPath,paste0("Plots/",myLabel,".png"),
                             sep ="/"), width = width, height = height)
        referenceSampleBarplot
        invisible(dev.off())
      }
    }
  }
  return(referenceSampleBarplot)
}
