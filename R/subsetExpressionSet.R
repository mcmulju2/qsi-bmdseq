#' Return a filtered matrix of expression data from an Expression Set
#'
#' Helper method in BMDseq designed to filter an Expression Set object down
#' to only the data related to a list of candidate Ensemble IDs
#'
#' @param expressionSet ExpressionSet object to filter
#' @param ensemblCandidates A vector of Ensembl IDs to search in the ExpressionSet
#' @return A list of matrices of filtered data untransformed[1], scaled[2]
#' @export
#'

subsetExpressionSet <- function(expressionSet, ensemblCandidates){
  require(Biobase)
  #1 - extract expression set from ExpressionSet object
  expressionMatrix <- exprs(expressionSet)
  #2 - List of Candidate Ensemble IDs
  uniqueCandidates <- unique(candidatesList)
  #3 - Keep only the intersection of the expression set and the candidate list
  ensemblFilter <- match(intersect(x = rownames(expressionMatrix),
                                   y = uniqueCandidates),
                         uniqueCandidates)
  #4 - Record any candidates that were lost
  notFound <- setdiff(x = uniqueCandidates, y = uniqueCandidates[ensemblFilter])
  #5 - restrict expression set ensembleIDs to only unique candidate list
  uniqueFilteredCandidates <- uniqueCandidates[ensemblFilter]
  #6 - filter and return the Candidate gene expression set
  filteredExpressionMatrix <- expressionMatrix[uniqueFilteredCandidates,]

  normalizedFilteredExpressionMatrix <- t(scale(t(filteredExpressionMatrix)))

  return(list(base = filteredExpressionMatrix,
              norm = normalizedFilteredExpressionMatrix,
              notFound = notFound,
              finalCandidateList = uniqueFilteredCandidates))
}
