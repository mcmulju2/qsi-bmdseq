#' Check a matrix for correct count data formatting, correct common problems, and
#' pass matrix to STS2 ID parser function
#'
#' Helper method in BMDseq designed to check a count data matrix for common pre-processing
#' issues, perform the necessary adjustments, then pass the matrix to the STS2 ID parser
#' for final return in cleaned format.
#'
#' @param countMatrix the data matrix with non-conforming format (matrix)
#' @param geneID string matching Ensembl gene IDs column header (string)
#' @param makeNumeric bool should the function coerce values to numerics (F = integer)
#' @return A correctly formatted counts data matrix ready for DESeq2 use.
#' @export

cleanCountMatrixFromImport <- function(countMatrix, geneID = "id", makeNumeric = T) {

  # Checking if EnsGene IDs are present as a column, and shift them to rownames if so.
  returnMatrix <- moveEnsembleIDsToRownames(countMatrix = countMatrix, geneID = geneID)

  #Parse out STS2 IDs from column headers.  Also checks that the str_extract resulted in
  #valid STS2 ID patterns.
  returnMatrix <- parseSTS2IDsFromColumnHeaders(countMatrix = returnMatrix)

  #Check if count matrix contains only numeric data.  If not, try to convert to numeric
  #If numeric conversion results in NAs, stops execution and issues error message.
  if(sum(apply(X = returnMatrix, MARGIN = 2, FUN = is.numeric))!= ncol(returnMatrix)){
    namePreserve <- dimnames(returnMatrix)
    returnMatrix <- apply(X = returnMatrix, MARGIN = 2,
                          FUN = ifelse(makeNumeric, as.numeric, as.integer))
    dimnames(returnMatrix) <- namePreserve
  }

    #Sanity check: did any values change when converted to integers
    ## TODO: Add a check to compare contents before and after conversion

    #Sanity check: Are there any columns full of NA values now?
    if(sum(apply(X = returnMatrix, MARGIN = 2, FUN = function(x) all(is.na(x))))>0){
      stop(... = "At least 1 column contains all NAs after type conversion. Stopping script...\n",
           ... = "Check count matrix for columns containing something other than counts.\n")
    }

  return(returnMatrix)
}
