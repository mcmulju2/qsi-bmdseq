#' Loop through a list of file names and combine their contents into one dataframe
#'
#' Helper method in BMDseq designed to take a list of file names at a given location
#' to loop over them, check their names for consistency, and merge or rbind them
#' into one return dataframe.
#'
#' @param dataFilePath path to annotation files on Linux server
#' @param annotationFileList list of file names to be found at dataFilePath
#' @param checkNames bool; should names of files be checked during import. Default T
#' @param extendNAs bool; should extended definition of 'NA' be used. Default T
#' @param removeNAs bool; should NAs be removed.  Default T#'
#' @return returnAnnotationFrame - the modified annotation data frame



combineDataFramesFromFileList <- function(dataFilePath, annotationFileList,
                                          checkNames = T, extendNAs = TRUE,
                                          removeNAs = TRUE, ...){

  # Assemble cumulative Annotation frame using either rbind or merge
  for(index in 1:length(annotationFileList)){
    if(index == 1){
      returnAnnotationFrame <- data.frame(fileFromPath(dataFilePath = dataFilePath,
                                                       dataFileName = annotationFileList[index],
                                                       check.names = checkNames,
                                                       extend.NAs = extendNAs,
                                                       remove.NAs = removeNAs),
                                          stringsAsFactors = FALSE)
    }else{
      readFile <- data.frame(fileFromPath(dataFilePath = dataFilePath,
                                          dataFileName = annotationFileList[index],
                                          check.names = checkNames,
                                          extend.NAs = extendNAs,
                                          remove.NAs = removeNAs),
                             stringsAsFactors = FALSE)

      if(mergeRequired){
        #Merge annotation files here
        returnAnnotationFrame <- merge(x = returnAnnotationFrame, y = readFile,
                                       all = T, sort = F)

      }else{
        #rbind annotation files here
        returnAnnotationFrame <- rbind(returnAnnotationFrame, readFile)
      }
    }
  }

  return(returnAnnotationFrame)
}
