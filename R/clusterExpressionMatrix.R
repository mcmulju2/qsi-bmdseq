#' Perform clustering of both rows and columns and return the respective objects
#'
#' Helper method in BMDseq designed to perform clustering by both rows and columns
#' and return the packaged objects as a list
#'
#' @param expressionMatrix Matrix of expression data (feature rows, subject columns)
#' @param clusterMethod String reference to clustering algorithm see hclust method
#' @return A list containing row-wise clustering[1], and column-wise clustering[2]
#' @export

clusterExpressionMatrix <- function(expressionMatrix, clusterMethod = "ward.D2"){
  #Hierarchical Clustering of rows
  clusterRows <- hclust(d = dist(expressionMatrix), clusterMethod)
  #Hierarchical Clustering of subjects
  clusterCols <- hclust(d = dist(t(expressionMatrix)), clusterMethod)

  return(list(rowClust = clusterRows, colClust = clusterCols))
}
